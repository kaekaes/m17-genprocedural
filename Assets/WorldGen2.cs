﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.Tilemaps;

public class WorldGen2: MonoBehaviour {

    public Sprite[] sprites;
    [System.Serializable]
    public class Ore {
        public string Name;
        public float probabilitat;
        [SerializeField] public GMinMax<int> minMaxHeight;
        public Sprite sprite;

        [System.Serializable]
        [SerializeField]
        public abstract class GMinMax<T> : PropertyDrawer {
            [SerializeField] public T min;
            [SerializeField] public T max;
            public GMinMax(T min, T max) {
                this.min = min;
                this.max = max;
            }

        }
    }
    public Ore[] ores;

    public Tilemap tilemap;
    public Tile coso;
    public int seed;
    public int sizeX, sizeY;

    [Header("Mountains")]
    public int mOctaves = 4;
    public float mFrequency = 2;
    public float mRough = 1.8f;
    public float mElevation = 7.45f;

    [Header("Caves")]
    public int caveStart = 15;
    public int caveLerp = 10;
    public float cFrequency = 2;
    public float cRough = 1.8f;
    public float cElevation = 7.45f;
    public float cThreshold = 3;
    public float cThresholdOffset = .05f;
    public float cThresholdOffsetBottom = .05f;

    public bool autoUpdate = false;

    void Start() {
        autoUpdate = true;
        Generate();

    }

    public void Generate() {
        tilemap.ClearAllEditorPreviewTiles();
        tilemap.ClearAllTiles();

        for (int x = 0; x < sizeX; x++) {
            float y = 0, r = 1, f = 1, totalR = 0;
            for (int oct = 0; oct < mOctaves; oct++) {
                //print("x:" + x + " | Oct:" + oct + " - yBase:" + y);
                y += r * Mathf.Sin(( x + seed ) / f) * mElevation;
                totalR += r;
                r *= mRough;
                f *= mFrequency;
            }
            y /= totalR;


            for (int i = 0; i < ( sizeY + Mathf.CeilToInt(y) ); i++) {


                if (i < sizeY + y - caveStart) {
                    float perl = Mathf.PerlinNoise(( x + seed ) / cFrequency, i / cFrequency) * cElevation;

                    if (i < sizeY + y - caveStart && i > sizeY + y - ( caveStart + caveLerp )) {
                        perl -= cThresholdOffset * ( Mathf.CeilToInt(i - ( sizeY + y - ( caveStart + caveLerp ) )) / ( caveLerp * 1f ) );
                        //print(i+"|"+( i - ( sizeY + y - ( caveStart + caveLerp ) ) ) / ( caveLerp * 1f ));
                    }

                    if (i < caveLerp + ( y / 2 )) {
                        //coso.color = Color.blue;
                        //print(i + ":" + ( ( ( caveLerp + y ) - i)/(caveLerp*1f)));
                        perl -= cThresholdOffsetBottom * ( Mathf.CeilToInt(( ( caveLerp + y ) - i ) / ( caveLerp * 1f )) );
                    }


                    if (perl > cThreshold)
                        continue;
                }
                //float perl = Mathf.PerlinNoise(x / mFrequency, (y + seed) / mFrequency) * mElevation;// * ((x + 1f) / resolution*afección)+offset;

                Vector3Int pos = new Vector3Int(x, i, 0);

                tilemap.SetTile(pos, coso);
                if (i >= sizeY + y - 1) {
                    Color c = tilemap.GetColor(pos);
                    c = Color.black;
                    coso.sprite = sprites[0];
                } else if (i >= sizeY + y - ( caveStart + ( caveLerp / Mathf.Clamp(
                    ( Mathf.Sin(( x + seed ) * 2 / cFrequency) ) * 0.7f + ( Mathf.Sin(( x * 2 + seed ) * 2 / ( cFrequency * 2 )) + 1 ) * 0.3f, 1, 2) ) )) {
                    coso.sprite = sprites[1];
                } else {
                    coso.sprite = sprites[2];
                }
            }
            coso.sprite = sprites[3];
            tilemap.SetTile(new Vector3Int(x, -1, 0), coso);

        }
    }

    public void Move(int v) {
        seed += v;
        Generate();
    }
}
