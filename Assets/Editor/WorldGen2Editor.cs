﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(WorldGen2))]
public class WorldGen2Editor : Editor{

	WorldGen2 wg2;

	public override void OnInspectorGUI() {
		using (var check = new EditorGUI.ChangeCheckScope()) {
			base.OnInspectorGUI();
			if (check.changed && wg2.autoUpdate) {
				wg2.Generate();
			}
		}

		if (GUILayout.Button("Regenerar")) {
			wg2.Generate();
		}

	}

	private void OnEnable() {
		wg2 = (WorldGen2)target;
	}

	}
